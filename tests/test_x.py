import unittest
from unittest.mock import patch, MagicMock
from x import validate_user_name, validate_user_last_name


class test_validate_user_name(unittest.TestCase):

    @patch('x.request')
    def test_validate_user_name_valid(self, mock_request):
        # Setup the mock request object
        mock_request.forms.get = MagicMock(return_value='validusername')
        # Call the function
        result = validate_user_name()
        # Assert the expected result
        self.assertEqual(result, 'validusername')

    @patch('x.request')
    def test_validate_user_name_too_short(self, mock_request):
        # Setup the mock request object
        mock_request.forms.get = MagicMock(return_value='a')
        # Assert that an exception is raised
        with self.assertRaises(Exception) as context:
            validate_user_name()

        self.assertEqual(context.exception.args[0], 400)
        self.assertEqual(context.exception.args[1], 'user_name 2 to 20 characters')

    @patch('x.request')
    def test_validate_user_name_too_long(self, mock_request):
        # Setup the mock request object
        mock_request.forms.get = MagicMock(return_value='aaaaaaaaaaaaaaaaaaaaaaa')
        # Assert that an exception is raised
        with self.assertRaises(Exception) as context:
            validate_user_name()

        self.assertEqual(context.exception.args[0], 400)
        self.assertEqual(context.exception.args[1], 'user_name 2 to 20 characters')


class test_validate_user_last_name(unittest.TestCase):
    @patch('x.request')
    def test_validate_user_last_name_valid(self, mock_request):
        # Setup the mock request object
        mock_request.forms.get = MagicMock(return_value='validlastname')
        # Call the function
        result = validate_user_last_name()
        # Assert the expected result
        self.assertEqual(result, 'validlastname')

    @patch('x.request')
    def test_validate_user_last_name_too_short(self, mock_request):
        # Setup the mock request object
        mock_request.forms.get = MagicMock(return_value='a')
        # Assert that an exception is raised
        with self.assertRaises(Exception) as context:
            validate_user_last_name()

        self.assertEqual(context.exception.args[0], 400)
        self.assertEqual(context.exception.args[1], 'user_last_name 2 to 20 characters')

    @patch('x.request')
    def test_validate_user_last_name_too_long(self, mock_request):
        # Setup the mock request object
        mock_request.forms.get = MagicMock(return_value='a' * 21)  # too long last name
        # Assert that an exception is raised
        with self.assertRaises(Exception) as context:
            validate_user_last_name()

        self.assertEqual(context.exception.args[0], 400)
        self.assertEqual(context.exception.args[1], 'user_last_name 2 to 20 characters')
