from bottle import get, static_file, run
import routes.index # noqa
import routes.get_crime # noqa
import api_tests.api_test #noqa


@get('/app.css')
def _():
    return static_file('app.css', root='.')


@get('/mixhtml.js')
def _():
    return static_file('mixhtml.js', root='.')


run(host="0.0.0.0", port=8080, debug=True, reloader=True)
