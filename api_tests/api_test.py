from bottle import request, post, HTTPResponse
import x
import json
import random
from datetime import datetime, date
from faker import Faker
from faker.providers import BaseProvider


# Custom provider for generating short crime descriptions
class CrimeDescriptionProvider(BaseProvider):
    def crime_description(self):
        genders = ["Male", "Female", "Gang"]
        actions = [
            "shot", "stabbed", "stole",
            "bombed", "vandalized", "assaulted",
            "robbed", "harassed"
        ]
        targets = [
            "dog", "cat", "bike",
            "car", "store", "bank",
            "restaurant", "house", "office",
            "street"
        ]

        gender = random.choice(genders)
        return f"{gender} {random.choice(actions)} {random.choice(targets)}"


# Custom provider for generating names based on gender
class CustomNameProvider(BaseProvider):
    def custom_first_name(self, gender):
        if gender == "Male":
            return fake.first_name_male()
        elif gender == "Female":
            return fake.first_name_female()


class CrimeDateProvider(BaseProvider):
    def crime_timestamp(self):
        current_date = date.today()
        current_year = current_date.year
        start_year = current_year - 1

        faker_date = fake.date_time_between_dates(datetime(start_year, 1, 1), current_date)
        return int(faker_date.timestamp())


fake = Faker("da")

fake.add_provider(CrimeDescriptionProvider)
fake.add_provider(CustomNameProvider)
fake.add_provider(CrimeDateProvider)


def generate_suspect(gender):
    if gender == "Gang":
        gender = random.choice(["Male", "Female"])  # Choose either Male or Female

    # Generate a new suspect
    first_three = random.randint(100, 999)
    if gender == "Male":
        last_digit = random.choice([1, 3, 5, 7, 9])
    else:
        last_digit = random.choice([0, 2, 4, 6, 8])

    birthdate = fake.profile("birthdate")
    birthdate_formatted = birthdate['birthdate'].strftime('%d%m%y')
    new_suspect_cpr = str(birthdate_formatted + str(first_three) + str(last_digit))

    new_suspect_firstname = fake.custom_first_name(gender)
    new_suspect_lastname = fake.last_name()
    new_suspect = {
        "cpr": new_suspect_cpr, "firstname": new_suspect_firstname,
        "lastname": new_suspect_lastname, "gender": gender
    }
    return new_suspect


def generate_suspects_api(new_gender):
    # Generate number of suspects based on gender
    num_suspects = 1 if new_gender != "Gang" else random.randint(2, 4)
    suspects = []
    for _ in range(num_suspects):
        new_suspect = generate_suspect(new_gender)
        suspects.append(new_suspect)

    return suspects


def validate_keys(keys, valid_keys):
    for key, value in keys.items():
        if key not in valid_keys:
            raise Exception(f"Invalid key: '{key}'", 400)
        if not value:
            raise Exception(f"'{key}' cannot be empty", 400)


def validate_coord_value(key, value):
    if not (
        len(value) == 5 and
        value[:2].isdigit() and
        value[2] == '.' and
        value[3:].isdigit()
    ):
        raise Exception(f"Invalid '{key}'. Use 2 float numbers, e.g. 12.55", 400)
    return True


def check_post_keys(keys):
    try:
        # Throw error if an invalid key is used in body
        valid_keys = {"gender", "lon", "lat", "description", "severity", "timestamp"}
        validate_keys(keys, valid_keys)

        valid_genders = ["Male", "Female", "Gang"]
        current_date = date.today()
        current_year = current_date.year
        start_year = current_year - 1
        start_timestamp = int(datetime(start_year, 1, 1).timestamp())
        current_timestamp = int(datetime.combine(current_date, datetime.min.time()).timestamp())

        # Check if data is present + is valid
        new_gender = keys["gender"]
        if new_gender not in valid_genders:
            raise Exception("Invalid 'gender'. Must be: 'Male', 'Female' or 'Gang'.", 400)

        new_lon = keys["lon"]
        validate_coord_value("lon", new_lon)

        if not (12.50 <= float(new_lon) <= 12.59):
            raise Exception("Invalid 'lon'. Must be between: 12.50 - 12.59.", 400)

        new_lat = keys["lat"]
        validate_coord_value("lat", new_lat)

        if not (55.60 <= float(new_lat) <= 55.69):
            raise Exception("Invalid 'lat'. Must be between: 55.60 - 55.69.", 400)

        new_description = keys["description"]
        new_description_words = new_description.split()
        if len(new_description_words) > 3 or len(new_description) > 100:
            raise Exception("Invalid 'description'. Must be max. 3 words and 100 characters.", 400)

        new_severity = keys["severity"]
        if int(new_severity) > 10 or int(new_severity) < 1:
            raise Exception("Invalid 'severity'. Must be between 1 - 10", 400)

        new_timestamp = keys["timestamp"]
        if start_timestamp > int(new_timestamp) or current_timestamp < int(new_timestamp):
            timestamp_string = f"Invalid 'timestamp'. Must be {start_timestamp}-{current_timestamp}"
            raise Exception(timestamp_string, 400)

    except Exception as e:
        error_message, error_code = e.args[0], e.args[1]

        # construct the JSON response
        jsonres = json.dumps({
            "message": error_message,
            "code": error_code
        })
        return HTTPResponse(
            status=error_code,
            body=jsonres,
            headers={"Content-Type": "application/json"}
        )

    return keys


@post("/create-new-crime")
def _():
    keys = {
        "gender": request.forms.get("gender", ""),
        "lon": request.forms.get("lon", ""),
        "lat": request.forms.get("lat", ""),
        "description": request.forms.get("description", ""),
        "severity": request.forms.get("severity", ""),
        "timestamp": request.forms.get("timestamp", "")
    }
    # Checks POST keys, returns dictionary if successful, returns httpresponse if not
    result = check_post_keys(keys)
    if isinstance(result, HTTPResponse):
        return result

    result["suspects"] = generate_suspects_api(random.choice(["Male", "Female", "Gang"]))
    id_pair = {
        "id": str(random.randint(1000, 9999))
    }

    # Adds id to top of dict
    new_entry = {**id_pair, **result}

    try:
        query = {
            "query": """
                INSERT @new_entry INTO crimes
            """,
            "bindVars": {"new_entry": new_entry},
        }
        res = x.db(query)
        if res.get("error"):
            raise Exception(res["errorMessage"], res["code"])
    except Exception as e:
        error_message, error_code = e.args[0], e.args[1]

        # construct the JSON response
        jsonres = json.dumps({
            "message": error_message,
            "code": error_code
        })
        return HTTPResponse(
            status=error_code,
            body=jsonres,
            headers={"Content-Type": "application/json"}
        )

    return HTTPResponse(status=200, body=json.dumps({
        "message": "Inserted new crime successfully",
        "code": 200,
        "data": new_entry
    }), headers={"Content-Type": "application/json"})
