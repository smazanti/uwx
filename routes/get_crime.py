from bottle import get, template, response, HTTPResponse
import json
import x
import requests


def check_collections():
    collections_to_check = [
        {"name": "crimes", "type": "document"},
        {"name": "crimes_commited_by_suspects", "type": "edge"},
        {"name": "suspects", "type": "document"},
    ]

    checked_collections = 0

    try:
        for collection in collections_to_check:
            res = x.db(collection, "collection")
            if res["error"] and res["errorMessage"] == "duplicate name":
                checked_collections += 1
    except Exception as e:
        print(f"Error checking collections: {e}")
        return False

    return checked_collections == len(collections_to_check)


@get("/drop-database")
def _():
    collections_to_check = [
        {"name": "crimes", "type": "document"},
        {"name": "crimes_commited_by_suspects", "type": "edge"},
        {"name": "suspects", "type": "document"},
    ]

    try:
        for collection in collections_to_check:
            query = {
                "query": """
                    FOR doc IN @@collection
                    REMOVE doc in @@collection
                """,
                "bindVars": {"@collection": collection["name"]},
            }
            res = x.db(query)
            if res["error"]:
                raise Exception(res["errorMessage"], res["code"])
    except Exception as e:
        error_message, error_code = e.args[0], e.args[1]

        # construct the JSON response
        jsonres = json.dumps({
            "message": error_message,
            "code": error_code
        })
        return HTTPResponse(
            status=error_code,
            body=jsonres,
            headers={"Content-Type": "application/json"}
        )

    return HTTPResponse(status=200, body=json.dumps({
        "message": "Database dropped successfully",
        "code": 200
    }), headers={"Content-Type": "application/json"})


@get("/crimes")
def _():
    if check_collections():
        try:
            query = {
                "query": """
                    FOR crime IN crimes
                    RETURN crime
                """
            }
            res = x.db(query)
            if res["error"]:
                raise Exception(res["errorMessage"], res["code"])
            elif len(res["result"]) == 0:
                response.status = 404
                return "No crimes found. try /update-crimes"
            else:
                response.content_type = 'application/json'
                response.status = 200
                return json.dumps(res["result"])
        except Exception as e:
            error_message, error_code = e.args[0], e.args[1]

            # construct the JSON response
            jsonres = json.dumps({
                "message": error_message,
                "code": error_code
            })

            return HTTPResponse(
                status=error_code,
                body=jsonres,
                headers={"Content-Type": "application/json"}
            )

    else:
        return HTTPResponse(
            status=404,
            body=json.dumps({
                "message": "Collections were missing. Please refresh the page.",
                "code": 404
            }),
            headers={"Content-Type": "application/json"}
        )


@get("/crimes/<id>")
def _(id):
    try:
        query = {
            "query": """
                FOR crime IN crimes
                FILTER crime.id == @id
                RETURN crime
            """,
            "bindVars": {"id": id},
        }
        res = x.db(query)
        if res["error"]:
            raise Exception(res["errorMessage"], res["code"])
        elif len(res["result"]) == 0:
            return HTTPResponse(status=404, body=json.dumps({
                "message": "No crimes found",
                "code": 404
            }), headers={"Content-Type": "application/json"})
        else:
            response.status = 200
            return template("_crime", crime=res["result"][0])

    except Exception as e:
        error_message, error_code = e.args[0], e.args[1]

        # construct the JSON response
        jsonres = json.dumps({
            "message": error_message,
            "code": error_code
        })

        return HTTPResponse(
            status=error_code,
            body=jsonres,
            headers={"Content-Type": "application/json"}
        )


@get("/pyanywhere-test")
def _():
    try:
        response = requests.get("https://magndut.pythonanywhere.com/crimes")
        if not response:
            raise Exception("Error: Could not connect to pythonanywhere server", 500)
        crimes = response.json()

        if not crimes:
            raise Exception("Error: No data found on pythonanywhere server", 404)

    except Exception as e:
        error_message, error_code = e.args[0], e.args[1]

        # construct the JSON response
        response = json.dumps({
            "message": error_message,
            "code": error_code
        })

        return HTTPResponse(
            status=error_code,
            body=response,
            headers={"Content-Type": "application/json"}
        )

    return HTTPResponse(status=200, body=json.dumps({
        "message": "Connected to pythonanywhere server",
        "code": 200,
        "data": crimes
    }), headers={"Content-Type": "application/json"})


@get("/update-crimes")
def _():
    try:
        response = requests.get("https://magndut.pythonanywhere.com/crimes")
        if not response:
            raise Exception("Error: Could not get crimes from server", 500)
        crimes = response.json()

        for crime in crimes:
            query = {
                "query": """
                    UPSERT { id: @crime.id }
                    INSERT @crime
                    REPLACE @crime IN crimes RETURN NEW
                """,
                "bindVars": {"crime": crime},
            }
            res = x.db(query)
            if res["error"]:
                raise Exception(res["errorMessage"], res["code"])

            crimeId = res["result"][0]["_id"]

            # Iterate over all suspects
            for suspect in res["result"][0]["suspects"]:
                query = {
                    "query": """
                        UPSERT { cpr: @suspect.cpr }
                        INSERT @suspect
                        REPLACE @suspect IN suspects RETURN NEW
                    """,
                    "bindVars": {"suspect": suspect},
                }

                res = x.db(query)
                if res["error"]:
                    raise Exception(res["errorMessage"], res["code"])

                suspectId = res["result"][0]["_id"]

                doc = {"_from": crimeId, "_to": suspectId}

                query = {
                    "query": """
                        UPSERT { _from: @doc._from, _to: @doc._to }
                        INSERT @doc
                        REPLACE @doc IN crimes_commited_by_suspects RETURN NEW
                    """,
                    "bindVars": {
                        "doc": doc,
                    },
                }
                res = x.db(query)
                if res["error"]:
                    raise Exception(res["errorMessage"], res["code"])

    except Exception as e:
        error_message, error_code = e.args[0], e.args[1]

        # construct the JSON response
        response = json.dumps({
            "message": error_message,
            "code": error_code
        })

        return HTTPResponse(
            status=error_code,
            body=response,
            headers={"Content-Type": "application/json"}
        )

    return HTTPResponse(
        status=200,
        body={"message": f"Successfully updated {len(crimes)} crimes", "code": 200},
        headers={"Content-Type": "application/json"}
    )
