#!/bin/sh

echo "Starting entrypoint file."
# python app.py

# python manage.py check
# python manage.py makemigrations
# python manage.py migrate

case "$RTE" in
    dev )
        echo "** Development mode."
        pip-audit
        coverage run --source="." --omit=app.py app.py test --verbosity 2
        coverage report -m
        python app.py
        ;;
    test )
        echo "** Test mode."
        pip-audit || exit 1
        coverage run --source="." --omit=app.py app.py test --verbosity 2
        coverage report -m --fail-under=80
        ;;
    prod )
        echo "** Production mode."
        pip-audit || exit 1
        # python manage.py check --deploy
	python app.py
        ;;
esac
